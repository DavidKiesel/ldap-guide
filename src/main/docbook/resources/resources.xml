<?xml version="1.0" encoding="UTF-8"?>
<section
    xml:id="Resources"
    xmlns="http://docbook.org/ns/docbook"
    version="5.0"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:xi="http://www.w3.org/2001/XInclude"
>
    <title>Resources</title>

    <para>
        A list of primary references is given below.
    </para>

    <itemizedlist>
        <listitem>
            <para>
                <link
                xlink:href="https://tools.ietf.org/html/rfc4510"><acronym>IETF</acronym>
                <acronym>RFC</acronym> 4510 - Lightweight Directory Access
                Protocol (<acronym>LDAP</acronym>): Technical Specification
                Road Map</link>
            </para>

            <itemizedlist>
                <listitem>
                    <para>
                        Contains a list of <acronym>IETF</acronym>
                        <acronym>RFC</acronym>s (with hyperlinks) that describe
                        aspects of <acronym>LDAP</acronym>.
                    </para>
                </listitem>
            </itemizedlist>
        </listitem>

        <listitem>
            <para>
                <link
                xlink:href="https://tools.ietf.org/html/rfc4519"><acronym>IETF</acronym>
                <acronym>RFC</acronym> 4519 - Lightweight Directory Access
                Protocol (<acronym>LDAP</acronym>): Schema for User
                Applications</link>
            </para>

            <itemizedlist>
                <listitem>
                    <para>
                        Contains a basic list of attribute types and object
                        classes.
                    </para>
                </listitem>
            </itemizedlist>
        </listitem>

        <listitem>
            <para>
                <link
                xlink:href="https://tools.ietf.org/html/rfc4517"><acronym>IETF</acronym>
                <acronym>RFC</acronym> 4517 - Lightweight Directory Access
                Protocol (<acronym>LDAP</acronym>): Syntaxes and Matching Rules
                </link>
            </para>

            <itemizedlist>
                <listitem>
                    <para>
                        Each syntax constrains the values that an attribute
                        type can have.  Each matching rule indicates how
                        matches are performed on the values of an attribute
                        type.
                    </para>
                </listitem>
            </itemizedlist>
        </listitem>

        <listitem>
            <para>
                <link
                xlink:href="https://tools.ietf.org/html/rfc2254"><acronym>IETF</acronym>
                <acronym>RFC</acronym> 2254 - The String Representation of
                <acronym>LDAP</acronym> Search Filters</link>
            </para>

            <itemizedlist>
                <listitem>
                    <para>
                        Syntax of <acronym>LDAP</acronym> search filters.
                    </para>
                </listitem>
            </itemizedlist>
        </listitem>

        <listitem>
            <para>
                <link
                xlink:href="https://www.iana.org/assignments/ldap-parameters">Lightweight
                Directory Access Protocol (<acronym>LDAP</acronym>)
                Parameters</link>
            </para>

            <itemizedlist>
                <listitem>
                    <para>
                        Collection of lists of various types of
                        <acronym>LDAP</acronym> parameters: object identifier
                        descriptors, <literal>resultCode</literal> values,
                        <acronym>LDAP</acronym> Syntaxes, etc.  Generally, each
                        list contains a name or description, a code or
                        <acronym>OID</acronym>, and a reference.
                    </para>
                </listitem>
            </itemizedlist>
        </listitem>
    </itemizedlist>

    <para>
        A list of vendor/application references is given below.
    </para>

    <itemizedlist>
        <listitem>
            <para>
                <link xlink:href="https://docs.microsoft.com/en-us/windows/desktop/adschema/classes-all">Microsoft Active Directory object classes</link>
            </para>

            <itemizedlist>
                <listitem>
                    <para>
                        List of Microsoft Active Directory object classes.
                    </para>
                </listitem>
            </itemizedlist>
        </listitem>

        <listitem>
            <para>
                <link xlink:href="https://docs.microsoft.com/en-us/windows/desktop/adschema/attributes-all">Microsoft Active Directory attributes</link>
            </para>

            <itemizedlist>
                <listitem>
                    <para>
                        List of Microsoft Active Directory attributes.
                    </para>
                </listitem>
            </itemizedlist>
        </listitem>

        <listitem>
            <para>
                <link
                xlink:href="https://docs.oracle.com/cd/E15586_01/oid.1111/e10029/toc.htm">Oracle®
                Fusion Middleware Administrator's Guide for Oracle Internet
                Directory 11g Release 1 (11.1.1)</link>
            </para>

            <itemizedlist>
                <listitem>
                    <para>
                        Oracle Internet Directory guide.
                    </para>
                </listitem>
            </itemizedlist>
        </listitem>

        <listitem>
            <para>
                <link
                xlink:href="https://access.redhat.com/documentation/en-us/red_hat_directory_server/10/">Product Documentation for Red Hat Directory Server 10</link>
            </para>

            <itemizedlist>
                <listitem>
                    <para>
                        Red Hat Directory Server documentation.
                    </para>
                </listitem>
            </itemizedlist>
        </listitem>

        <listitem>
            <para>
                <link
                xlink:href="https://www.openldap.org/">OpenLDAP</link>
            </para>

            <itemizedlist>
                <listitem>
                    <para>
                        OpenLDAP home page.  Links to guides and manual
                        pages.
                    </para>
                </listitem>
            </itemizedlist>
        </listitem>
    </itemizedlist>

    <para>
        A list of miscellaneous guides and references is given below.
    </para>

    <itemizedlist>
        <listitem>
            <para>
                <link xlink:href="https://en.wikipedia.org/wiki/Lightweight_Directory_Access_Protocol">Wikipedia LDAP article</link>
            </para>

            <itemizedlist>
                <listitem>
                    <para>
                        Wikipedia LDAP article.
                    </para>
                </listitem>
            </itemizedlist>
        </listitem>

        <listitem>
            <para>
                <link xlink:href="https://ldapwiki.com/wiki/Main">LDAP Wiki</link>
            </para>

            <itemizedlist>
                <listitem>
                    <para>
                        LDAP articles.
                    </para>
                </listitem>
            </itemizedlist>
        </listitem>

        <listitem>
            <para>
                <link xlink:href="https://www.tldp.org/HOWTO/LDAP-HOWTO/">LDAP Linux HOWTO</link>
            </para>

            <itemizedlist>
                <listitem>
                    <para>
                        The Linux Documentation Project (TLDP) how-to.
                    </para>
                </listitem>
            </itemizedlist>
        </listitem>

        <listitem>
            <para>
                <link xlink:href="https://ldap.com/">ldap.com</link>
            </para>

            <itemizedlist>
                <listitem>
                    <para>
                        Guides and reference materials.
                    </para>
                </listitem>
            </itemizedlist>
        </listitem>

        <listitem>
            <para>
                <link
                xlink:href="https://ldap.com/ldap-oid-reference-guide/">LDAP
                <acronym>OID</acronym> Reference Guide</link>
            </para>

            <itemizedlist>
                <listitem>
                    <para>
                        <acronym>OID</acronym> reference.
                    </para>
                </listitem>
            </itemizedlist>
        </listitem>

        <listitem>
            <para>
                <link
                xlink:href="http://oid-info.com/basic-search.htm"><acronym>OID</acronym>
                Repository Basic search</link>
            </para>

            <itemizedlist>
                <listitem>
                    <para>
                        <acronym>OID</acronym> reference.
                    </para>
                </listitem>
            </itemizedlist>
        </listitem>

        <listitem>
            <para>
                <link
                xlink:href="https://www.alvestrand.no/objectid/top.html">
                Alvestrand <acronym>OID</acronym> reference</link>
            </para>

            <itemizedlist>
                <listitem>
                    <para>
                        <acronym>OID</acronym> reference.
                    </para>
                </listitem>
            </itemizedlist>
        </listitem>
    </itemizedlist>

</section>

