<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet  
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0"
>
    <!-- imports ********************************************************* -->
    <xsl:import
        href="urn:docbkx:stylesheet/chunkfast.xsl"
    />

    <xsl:import
        href="urn:docbkx:stylesheet/highlight.xsl"
    />

    <!-- params ********************************************************** -->
    <!-- http://docbook.sourceforge.net/release/xsl/current/doc/html/ -->

    <!-- set css stylesheet -->
    <xsl:param
        name="html.stylesheet"
        select="'css/article.css'"
    />

    <!-- default is for book and chapters to generate toc -->
    <xsl:param
        name="generate.toc"
    >
        article    toc
    </xsl:param>

    <!-- default is 2  -->
    <xsl:param
        name="toc.section.depth"
    >
        6
    </xsl:param>

    <!-- set css stylesheet -->
    <xsl:param
        name="section.autolabel"
        select="1"
    />

    <!-- set chunking root filename (defaults to index) -->
    <xsl:param
        name="root.filename"
    >ar01</xsl:param>

    <!-- set chunking depth -->
    <xsl:param
        name="chunk.section.depth"
        select="3"
    />

    <!-- set chunking of first sections -->
    <xsl:param
        name="chunk.first.sections"
        select="1"
    />

    <!-- separate chunk for table of contents and list of titles -->
    <xsl:param
        name="chunk.tocs.and.lots"
        select="1"
    />

    <!-- enable code highlighting -->
    <xsl:param
        name="highlight.source"
        select="1"
    />

    <!-- modifications *************************************************** -->

    <xsl:param name="target.window" select="'right_frame'"/>
    <xsl:template name="user.head.content">
        <base target="{$target.window}"/>
    </xsl:template>

    <xsl:output
        method="html"
        encoding="UTF-8"
        indent="no"
    />
</xsl:stylesheet>
